import numpy as np
from scipy.stats import pearsonr

Array1 = [[1, 2, 3], [4, 5, 6]]
Array2 = [[11, 25, 346], [734, 48, 49]]
Mat1 = np.array(Array1)
Mat2 = np.array(Array2)
correlation = np.corrcoef(Mat1, Mat2)
print("矩阵1\n", Mat1)
print("矩阵2\n", Mat2)
print("相关系数矩阵\n", correlation)

pc = pearsonr(Mat1, Mat2)

print("相关系数：", pc[0])
print("显著性水平：", pc[1])
