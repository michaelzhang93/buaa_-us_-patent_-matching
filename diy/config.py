"""
配置参数信息
"""

Config = {
    'model_path': 'output',
    "pretrain_model_path": 'microsoft/deberta-v3-small',
    # "pretrain_model_path": '../bert-for-patents',
    # "pretrain_model_path": 'bert-base-uncased',
    "epoch": 5,
    "batch_size": 50,
    'train_data_path': 'new_data/train.csv',
    'valid_data_path': 'new_data/valid.csv',
    'test__data_path': 'new_data/test.csv',
    "learning_rate": 2e-5,
    "optimizer": 'adam',
    'out_size': 768,
}
