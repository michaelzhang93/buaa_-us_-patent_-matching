import numpy as np
import torch.nn as nn
from transformers import AutoModelForMaskedLM, AutoModel, BertModel
import torch
from torch.optim import Adam, SGD
from scipy.spatial.distance import pdist
from scipy.stats import pearsonr


class SentenceEncoder(nn.Module):
    def __init__(self, config):
        super(SentenceEncoder, self).__init__()
        if 'bert-' in config['pretrain_model_path'].split('/')[-1]:
            # self.encoder = AutoModelForMaskedLM.from_pretrained(config['pretrain_model_path'])
            self.encoder = BertModel.from_pretrained(config['pretrain_model_path'])
        else:
            self.encoder = AutoModel.from_pretrained(config['pretrain_model_path'])
        hidden_size = self.encoder.config.vocab_size
        # self.classify = nn.Linear(hidden_size, config['out_size'])

    def forward(self, x):
        x = self.encoder(x)[0]
        # y = nn.functional.max_pool1d(x.transpose(1, 2), x.shape[1]).squeeze()  # squeeze是把单维度的删除
        x = nn.functional.max_pool1d(x.transpose(1, 2), x.shape[1]).squeeze(-1)  # squeeze是把单维度的删除
        # x = self.classify(x)
        return x


class Torchmodel(nn.Module):
    def __init__(self, config):
        super(Torchmodel, self).__init__()
        self.sentence_encoder = SentenceEncoder(config)
        self.similarity = nn.CosineSimilarity()
        self.loss = nn.MSELoss()

    def forward(self, input, target=None, label=None):
        # pearson_count = pearsonr(input, target)
        if target is not None:
            input = self.sentence_encoder(input)
            target = self.sentence_encoder(target)
            # relation_score=torch.corrcoef(input,target)
            rel_score = self.similarity(input, target).unsqueeze(-1)
            if label is not None:  # 维度不同，rel_score为１，怎么算loss
                return self.loss(rel_score, label)
            else:
                return rel_score
        else:
            return self.sentence_encoder(input)


def choose_optimizer(config, model):
    optimizer = config["optimizer"]
    learning_rate = config["learning_rate"]
    if optimizer == "adam":
        return Adam(model.parameters(), lr=learning_rate)
    elif optimizer == "sgd":
        return SGD(model.parameters(), lr=learning_rate)
